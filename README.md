- actual blog post at [blog](https://ongspxm.gitlab.io/blog/2020/12/tutorial-ball-bounce-c/)
- inspired by [article](https://nullprogram.com/blog/2017/11/03/)
- repository at [gitlab](https://gitlab.com/ongspxm-blog/ball-bouncing-c)

## part 1 - displaying stuff in C

### PPM Portable Bitmap format
PPM P6 represent the [PPM P6](https://en.wikipedia.org/wiki/Netpbm#File_formats) format. This was chosen because its easy to just output plain text files, and get some software to display the shots. The mpv software was easy to use, have your script outputting P6 format, and then piping it into the software, just like so.

```bash
./a.out | mpv --fps=60 -
```

Or if we want to save the images into a video file, we could do this too.

```bash
./a.out | mpv --fps=60 --output=test.mp4 -
```

### version 1 - displaying 60 frames of 1x1 white tile
So what are we gonna to do here? First let's start by setting up all the code we need to get the display started.

```c
#include <stdio.h>
#include <stdlib.h>

#define S 1

static void frame() {
    unsigned char *f = malloc(3*S*S);
    printf("P6\n%d %d\n255\n", S, S);
    f[0] = 255; f[1] = 255; f[2] = 255;
    if (!fwrite(f, 3*S*S, 1, stdout)) exit(EXIT_FAILURE);
}

int main() {
    for (int i=0; i<60; i++) frame();
    return 0;
}
```

## part 2 - wrapping the display into an object 
What we want here is to create functions for us so that we can use the display much easier, so what's the idea here then? We want to have a buffer for us to draw into. And for function generalization, we want to have some kind of object that we can alter, draw stuff on. 

Here, we create a `frame` object, wrap everything in this object, and next step, we will be drawing stuff on it.

```c
#include <stdio.h>
#include <stdlib.h>

#define S 1

struct frame {
    int s;
    unsigned char d[];
};

struct frame *frame_new(int side) {
    struct frame *f = malloc(sizeof(*f) + 3*side*side);
    f->s = side; return f;
} 

static void display() {
    struct frame *f = frame_new(S);
    printf("P6\n%d %d\n255\n", f->s, f->s);
    if (!fwrite(f->d, 3*f->s*f->s, 1, stdout)) exit(EXIT_FAILURE);
}

int main() {
    for (int i=0; i<60; i++) display();
    return 0;
}
```

## part 3 - drawing the borders of display 
Okay so first shape to draw - rectangles. Here, I will be drawing the rectangles as the border of the frame. Here, to make the functions a little more generic, I will be using float values to represent the boundaries of the rectangle to draw, having function like so `static void draw_rect(struct frame *f, float x1, float x2, float y1, float y2)`.

```c
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define S 128 
#define w_border 0.005

struct frame {
    int s;
    unsigned char d[];
};

struct frame *frame_new(int side) {
    struct frame *f = malloc(sizeof(*f) + 3*side*side);
    f->s = side; return f;
}

static void draw_rect(struct frame *f, float x1, float x2, float y1, float y2) {
    assert(x1>=0 && x2<=1.0);
    assert(y1>=0 && y2<=1.0);

    for (int x=x1*f->s; x<x2*f->s; x++) {
        for (int y=y1*f->s; y<y2*f->s; y++) {
            f->d[3*(y*f->s+x)+0] = 255;
            f->d[3*(y*f->s+x)+1] = 255;
            f->d[3*(y*f->s+x)+2] = 255;
        }
    }
}

static void display() {
    struct frame *f = frame_new(S);
    draw_rect(f, 0, 1, 0, w_border);
    draw_rect(f, 0, 1, 1-w_border, 1);
    draw_rect(f, 0, w_border, 0, 1);
    draw_rect(f, 1-w_border, 1, 0, 1);

    printf("P6\n%d %d\n255\n", f->s, f->s);
    if (!fwrite(f->d, 3*f->s*f->s, 1, stdout)) exit(EXIT_FAILURE);
}

int main() {
    for (int i=0; i<60; i++) display();
    return 0;
}
```

## part 4 - simulating the ball 
Since we are gonna to make a simulation of bouncing ball, here is where we start to defining our game environment. The way we modify `display()`, we pass in an env and then do the "bouncing logic" on the ball. 

The environment includes both the `(x, y)` position of the ball, and the direction `(dx, dy)` that describes the movement. During the update, these values will be appended to the x-y position, and this will then "move the ball" to the right place.

```c
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define S 128 
#define PI 6.283185

#define r_ball 0.1
#define w_ball 0.001
#define w_border 0.005

/** FUNCS - DRAWING **/
struct frame {
    int s;
    unsigned char d[];
};

struct frame *frame_new(int side) {
    struct frame *f = malloc(sizeof(*f) + 3*side*side);
    f->s = side; return f;
}

static void draw_rect(struct frame *f, float x1, float x2, float y1, float y2) {
    assert(x1>=0 && x2<=1.0);
    assert(y1>=0 && y2<=1.0);

    for (int x=x1*f->s; x<x2*f->s; x++) {
        for (int y=y1*f->s; y<y2*f->s; y++) {
            f->d[3*(y*f->s+x)+0] = 255;
            f->d[3*(y*f->s+x)+1] = 255;
            f->d[3*(y*f->s+x)+2] = 255;
        }
    }
}

/** FUNCS - ENV **/
struct env {
    float x, y, dx, dy, r;
};

struct env *env_new() {
    struct env *e = malloc(sizeof(*e));
    e->x = (rand()%100)/100;
    e->y = (rand()%100)/100;

    float r = PI*(rand()%100)/100;
    e->dx = sin(r)*r_ball;
    e->dy = cos(r)*r_ball;
}

struct env *display(struct env *e) {
    struct frame *f = frame_new(S);
    draw_rect(f, 0, 1, 0, w_border);
    draw_rect(f, 0, 1, 1-w_border, 1);
    draw_rect(f, 0, w_border, 0, 1);
    draw_rect(f, 1-w_border, 1, 0, 1);

    // change direction
    if ((e->x-e->r < w_border && e->dy<0) || (e->x+e->r > 1-w_border && e->dy>0)) { e->dy *= -1; }
    if ((e->y-e->r < w_border && e->dx<0) || (e->y+e->r > 1-w_border && e->dx>0)) { e->dx *= -1; } 
    e->x += e->dx; e->y += e->dy;

    printf("P6\n%d %d\n255\n", f->s, f->s);
    if (!fwrite(f->d, 3*f->s*f->s, 1, stdout)) exit(EXIT_FAILURE);

    return e;
}

int main() {
    struct env *e = env_new();
    e->r = r_ball;

    for (int i=0; i<60; i++) e = display(e);
    return 0;
}
```

## part 5 - displaying the ball
Now that we have the environment all ready. its time to start drawing the ball. So here, we did a bit of refactoring, took out the part where you print dots on the frame. Maybe later on we can add color too.

The highlight of this portion is really the 2 for loops that checks for the circle and print it. And one thing good about this, if you increase the resolution of this, everything looks the same as well. But why is that so? Because all of our system uses the relative positioning, this makes scaling much much easier.

```c
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define DEBUG 0 
#define FRAME 200

#define S 128 
#define PI 6.283185

#define r_ball 0.05
#define w_ball 0.001
#define w_border 0.005

/** FUNCS - DRAWING **/
struct frame {
    int s;
    unsigned char d[];
};

struct frame *frame_new(int side) {
    struct frame *f = malloc(sizeof(*f) + 3*side*side);
    f->s = side; return f;
}

static void draw_dot(struct frame *f, int x, int y) {
    f->d[3*(y*f->s+x)+0] = 255;
    f->d[3*(y*f->s+x)+1] = 255;
    f->d[3*(y*f->s+x)+2] = 255;
} 

static void draw_rect(struct frame *f, float x1, float x2, float y1, float y2) {
    assert(x1>=0 && x2<=1.0);
    assert(y1>=0 && y2<=1.0);

    for (int x=x1*f->s; x<x2*f->s; x++) {
        for (int y=y1*f->s; y<y2*f->s; y++) {
            draw_dot(f, x, y);
        }
    }
}

static void draw_circle(struct frame *f, float xx, float yy, float rr) {
    int cx=xx*f->s, cy=yy*f->s, r=rr*f->s;
    for (int x=cx-r; x<cx+r; x++) {
        if (x<0 || x>f->s) continue;
        for (int y=cy-r; y<cy+r; y++) {
            if (y<0 || y>f->s) continue;
            if ((cx-x)*(cx-x) + (cy-y)*(cy-y) > r*r) continue;
            draw_dot(f, x, y);
        }
    } 
}

/** FUNCS - ENV **/
struct env {
    float x, y, dx, dy, r;
};

struct env *env_new() {
    struct env *e = malloc(sizeof(*e));
    e->r = r_ball;
    e->x = (rand()%100)/100;
    e->y = (rand()%100)/100;

    float r = PI*(rand()%100)/100;
    e->dx = sin(r)*r_ball;
    e->dy = cos(r)*r_ball;
}

static void display(struct env *e) {
    struct frame *f = frame_new(S);
    draw_rect(f, 0, 1, 0, w_border);
    draw_rect(f, 0, 1, 1-w_border, 1);
    draw_rect(f, 0, w_border, 0, 1);
    draw_rect(f, 1-w_border, 1, 0, 1);

    // change direction
    if ((e->y-e->r < w_border && e->dy<0) || (e->y+e->r > 1-w_border && e->dy>0)) { e->dy *= -1; }
    if ((e->x-e->r < w_border && e->dx<0) || (e->x+e->r > 1-w_border && e->dx>0)) { e->dx *= -1; } 
    e->x += e->dx; e->y += e->dy;
    draw_circle(f, e->x, e->y, e->r);

    if (!DEBUG) {
        printf("P6\n%d %d\n255\n", f->s, f->s);
        if (!fwrite(f->d, 3*f->s*f->s, 1, stdout)) exit(EXIT_FAILURE);
    }
}

int main() {
    struct env *e = env_new();
    for (int i=0; i<FRAME; i++) display(e);
    return 0;
}
```

### how it should look like up till now
<iframe width="560" height="315" src="https://www.youtube.com/embed/D1rcXspcjtE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## part 6 - grided to run multiple simulations
Here, we are gonna bring the project one step further. Instead of just drawing one simulation, we want to be displaying multiple simulations at the same time. And how do we do that?

Here, we introduce a new idea `view`, which is used to represent one small portion of the full frame. When the frame is created, we cut it into different sections. And the view is just a wrapper around the actual frame.

And since all our higher order functions uses relative postioning, it is not hard for us to split everything up, and run different environments for different views. And the logic that stacks the different views together, that sits in `draw_dot()`, the actual function does does the full frame drawing.

```c
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#define DEBUG 0
#define FRAME 200

#define SIZE 1280
#define PI 6.283185

#define v_ball 0.1
#define r_ball 0.05
#define w_ball 0.001
#define w_border 0.005

/** FUNCS - DRAWING **/
struct frame {
    int s, c;
    unsigned char d[];
};

struct view {
    int s, v;
    struct frame *f;
};

struct frame *frame_new(int side, int cuts) {
    struct frame *f = malloc(sizeof(*f) + 3*side*side);
    f->s = side; f->c = cuts; return f;
}

struct view *frame_view(struct frame *f, int viewid) {
    struct view *v = malloc(sizeof(*v));
    v->f = f; v->s = (f->s)/(f->c);
    v->v = viewid; return v;
}

static void draw_dot(struct view *v, int x, int y) {
    struct frame *f = v->f;
    int yy=((v->v)/(f->c))*v->s, xx=(v->v)%(f->c);
    f->d[3*((yy+y)*f->s+(xx*v->s)+x)+0] = 255;
    f->d[3*((yy+y)*f->s+(xx*v->s)+x)+1] = 255;
    f->d[3*((yy+y)*f->s+(xx*v->s)+x)+2] = 255;
}

static void draw_rect(struct view *v, float x1, float x2, float y1, float y2) {
    assert(x1>=0 && x2<=1.0);
    assert(y1>=0 && y2<=1.0);

    for (int x=x1*v->s; x<x2*v->s; x++) {
        for (int y=y1*v->s; y<y2*v->s; y++) {
            draw_dot(v, x, y);
        }
    }
}

static void draw_circle(struct view *f, float xx, float yy, float rr) {
    int cx=xx*f->s, cy=yy*f->s, r=rr*f->s;
    for (int x=cx-r; x<cx+r; x++) {
        if (x<0 || x>f->s) continue;
        for (int y=cy-r; y<cy+r; y++) {
            if (y<0 || y>f->s) continue;
            if ((cx-x)*(cx-x) + (cy-y)*(cy-y) > r*r) continue;
            draw_dot(f, x, y);
        }
    }
}

/** FUNCS - ENV **/
struct env {
    float x, y, dx, dy, r;
};

struct env *env_new() {
    struct env *e = malloc(sizeof(*e));
    e->r = r_ball;
    e->x = (rand()%100)/100;
    e->y = (rand()%100)/100;

    float r = PI*(rand()%100)/100;
    e->dx = sin(r)*v_ball;
    e->dy = cos(r)*v_ball;
}

static void display(struct env *e) {
    struct frame *f = frame_new(SIZE, 4);
    struct view *v = frame_view(f, 1);

    draw_rect(v, 0, 1, 0, w_border);
    draw_rect(v, 0, 1, 1-w_border, 1);
    draw_rect(v, 0, w_border, 0, 1);
    draw_rect(v, 1-w_border, 1, 0, 1);

    // change direction
    if ((e->y-e->r < w_border && e->dy<0)) { e->dy *= -1; e->y=w_border+e->r;}
    else if ((e->y+e->r > 1-w_border && e->dy>0)) { e->dy *= -1; e->y=1-w_border-e->r; }
    if ((e->x-e->r < w_border && e->dx<0)) { e->dx *= -1; e->x= w_border+e->r; }
    else if (e->x+e->r > 1-w_border && e->dx>0) { e->dx *= -1; e->x=1-w_border-e->r; }
    e->x += e->dx; e->y += e->dy;
    draw_circle(v, e->x, e->y, e->r);

    if (!DEBUG) {
        printf("P6\n%d %d\n255\n", f->s, f->s);
        if (!fwrite(f->d, 3*f->s*f->s, 1, stdout)) exit(EXIT_FAILURE);
    }
}

int main() {
    srand(time(0));
    struct env *e = env_new();
    for (int i=0; i<FRAME; i++) display(e);
    return 0;
}
```
### how it should look it (if you got everything right)
<iframe width="560" height="315" src="https://www.youtube.com/embed/N5z3WFDxU18" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## part 7 - running multiple simulations
So, we have been building up to running multiple ball simulations at the same time. We have the views slice, we have encapsulated all the values we needed for the simulation, now its time to just put everything together.

One view for one simulation. We also made some modifications to the ball size and speed so that we have a easier time tracking the movement. We can easily see that the simulations are quite different, all having different motions.

```c
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#define DEBUG 0

#define SLICE 4
#define FRAME 1280

#define SIZE 1280
#define PI 6.283185

#define v_ball 0.01
#define r_ball 0.02
#define w_ball 0.001
#define w_border 0.005

/** FUNCS - DRAWING **/
struct frame {
    int s, c;
    unsigned char d[];
};

struct view {
    int s, v;
    struct frame *f;
};

struct frame *frame_new(int side, int cuts) {
    struct frame *f = malloc(sizeof(*f) + 3*side*side);
    f->s = side; f->c = cuts; return f;
}

struct view *frame_view(struct frame *f, int viewid) {
    struct view *v = malloc(sizeof(*v));
    v->f = f; v->s = (f->s)/(f->c);
    v->v = viewid; return v;
}

static void draw_dot(struct view *v, int x, int y) {
    struct frame *f = v->f;
    int yy=((v->v)/(f->c))*v->s, xx=(v->v)%(f->c);
    f->d[3*((yy+y)*f->s+(xx*v->s)+x)+0] = 255;
    f->d[3*((yy+y)*f->s+(xx*v->s)+x)+1] = 255;
    f->d[3*((yy+y)*f->s+(xx*v->s)+x)+2] = 255;
}

static void draw_rect(struct view *v, float x1, float x2, float y1, float y2) {
    assert(x1>=0 && x2<=1.0);
    assert(y1>=0 && y2<=1.0);

    for (int x=x1*v->s; x<x2*v->s; x++) {
        for (int y=y1*v->s; y<y2*v->s; y++) {
            draw_dot(v, x, y);
        }
    }
}

static void draw_circle(struct view *f, float xx, float yy, float rr) {
    int cx=xx*f->s, cy=yy*f->s, r=rr*f->s;
    for (int x=cx-r; x<cx+r; x++) {
        if (x<0 || x>f->s) continue;
        for (int y=cy-r; y<cy+r; y++) {
            if (y<0 || y>f->s) continue;
            if ((cx-x)*(cx-x) + (cy-y)*(cy-y) > r*r) continue;
            draw_dot(f, x, y);
        }
    }
}

/** FUNCS - ENV **/
struct env {
    float x, y, dx, dy, r;
};

struct env *env_new() {
    struct env *e = malloc(sizeof(*e));
    e->r = r_ball;
    e->x = (rand()%100)/100;
    e->y = (rand()%100)/100;

    float r = PI*(rand()%100)/100;
    e->dx = sin(r)*v_ball;
    e->dy = cos(r)*v_ball;
}

static void display(struct view *v, struct env *e) {
    draw_rect(v, 0, 1, 0, w_border);
    draw_rect(v, 0, 1, 1-w_border, 1);
    draw_rect(v, 0, w_border, 0, 1);
    draw_rect(v, 1-w_border, 1, 0, 1);

    // change direction
    if ((e->y-e->r < w_border && e->dy<0)) { e->dy *= -1; e->y=w_border+e->r;}
    else if ((e->y+e->r > 1-w_border && e->dy>0)) { e->dy *= -1; e->y=1-w_border-e->r; }
    if ((e->x-e->r < w_border && e->dx<0)) { e->dx *= -1; e->x= w_border+e->r; }
    else if (e->x+e->r > 1-w_border && e->dx>0) { e->dx *= -1; e->x=1-w_border-e->r; }
    e->x += e->dx; e->y += e->dy;
    draw_circle(v, e->x, e->y, e->r);
}

int main() {
    srand(time(0));
    int slice = SLICE;

    struct env **envs = malloc(slice*slice*sizeof(struct env));
    for (int c=0; c<slice*slice; c++) envs[c] = env_new();

    for (int i=0; i<FRAME; i++) {
        struct frame *f = frame_new(SIZE, slice);

        for (int c=0; c<slice*slice; c++) {
            display(frame_view(f, c), envs[c]);
        }

        if (!DEBUG) {
            printf("P6\n%d %d\n255\n", f->s, f->s);
            if (!fwrite(f->d, 3*f->s*f->s, 1, stdout)) exit(EXIT_FAILURE);
        }
    }
    return 0;
}
```

### if all goes well
<iframe width="560" height="315" src="https://www.youtube.com/embed/bLT1NdPH7MI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## conclusion
So, that most of it for the ball bouncing project. A quick recap

- we went through the basics of displaying a PPM file
- refactored that we can easily alter the display
- created environment for us to run our ball simulation
- split frames in different views
- tie view to individual environments, running everything at once

[ballbounce.c](https://gitlab.com/ongspxm-blog/ball-bouncing-c/-/blob/master/script.c)
