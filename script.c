#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#define DEBUG 0

#define SLICE 4
#define FRAME 1280

#define SIZE 1280
#define PI 6.283185

#define v_ball 0.01
#define r_ball 0.02
#define w_ball 0.001
#define w_border 0.005

/** FUNCS - DRAWING **/
struct frame {
    int s, c;
    unsigned char d[];
};

struct view {
    int s, v;
    struct frame *f;
};

struct frame *frame_new(int side, int cuts) {
    struct frame *f = malloc(sizeof(*f) + 3*side*side);
    f->s = side; f->c = cuts; return f;
}

struct view *frame_view(struct frame *f, int viewid) {
    struct view *v = malloc(sizeof(*v));
    v->f = f; v->s = (f->s)/(f->c);
    v->v = viewid; return v;
}

static void draw_dot(struct view *v, int x, int y) {
    struct frame *f = v->f;
    int yy=((v->v)/(f->c))*v->s, xx=(v->v)%(f->c);
    f->d[3*((yy+y)*f->s+(xx*v->s)+x)+0] = 255;
    f->d[3*((yy+y)*f->s+(xx*v->s)+x)+1] = 255;
    f->d[3*((yy+y)*f->s+(xx*v->s)+x)+2] = 255;
}

static void draw_rect(struct view *v, float x1, float x2, float y1, float y2) {
    assert(x1>=0 && x2<=1.0);
    assert(y1>=0 && y2<=1.0);

    for (int x=x1*v->s; x<x2*v->s; x++) {
        for (int y=y1*v->s; y<y2*v->s; y++) {
            draw_dot(v, x, y);
        }
    }
}

static void draw_circle(struct view *f, float xx, float yy, float rr) {
    int cx=xx*f->s, cy=yy*f->s, r=rr*f->s;
    for (int x=cx-r; x<cx+r; x++) {
        if (x<0 || x>f->s) continue;
        for (int y=cy-r; y<cy+r; y++) {
            if (y<0 || y>f->s) continue;
            if ((cx-x)*(cx-x) + (cy-y)*(cy-y) > r*r) continue;
            draw_dot(f, x, y);
        }
    }
}

/** FUNCS - ENV **/
struct env {
    float x, y, dx, dy, r;
};

struct env *env_new() {
    struct env *e = malloc(sizeof(*e));
    e->r = r_ball;
    e->x = (rand()%100)/100;
    e->y = (rand()%100)/100;

    float r = PI*(rand()%100)/100;
    e->dx = sin(r)*v_ball;
    e->dy = cos(r)*v_ball;
}

static void display(struct view *v, struct env *e) {
    draw_rect(v, 0, 1, 0, w_border);
    draw_rect(v, 0, 1, 1-w_border, 1);
    draw_rect(v, 0, w_border, 0, 1);
    draw_rect(v, 1-w_border, 1, 0, 1);

    // change direction
    if ((e->y-e->r < w_border && e->dy<0)) { e->dy *= -1; e->y=w_border+e->r;}
    else if ((e->y+e->r > 1-w_border && e->dy>0)) { e->dy *= -1; e->y=1-w_border-e->r; }
    if ((e->x-e->r < w_border && e->dx<0)) { e->dx *= -1; e->x= w_border+e->r; }
    else if (e->x+e->r > 1-w_border && e->dx>0) { e->dx *= -1; e->x=1-w_border-e->r; }
    e->x += e->dx; e->y += e->dy;
    draw_circle(v, e->x, e->y, e->r);
}

int main() {
    srand(time(0));
    int slice = SLICE;

    struct env **envs = malloc(slice*slice*sizeof(struct env));
    for (int c=0; c<slice*slice; c++) envs[c] = env_new();

    for (int i=0; i<FRAME; i++) {
        struct frame *f = frame_new(SIZE, slice);

        for (int c=0; c<slice*slice; c++) {
            display(frame_view(f, c), envs[c]);
        }

        if (!DEBUG) {
            printf("P6\n%d %d\n255\n", f->s, f->s);
            if (!fwrite(f->d, 3*f->s*f->s, 1, stdout)) exit(EXIT_FAILURE);
        }
    }
    return 0;
}
